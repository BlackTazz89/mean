const util = require('util')
const node_acl = require('acl')
const mongo = require('mongodb').MongoClient
const url = 'mongodb://localhost:27017'

let acl = null

mongo.connect(url, { useNewUrlParser: true }, (err, client) => {
    if (err) {
        console.error(`Error during connection ${err}`)
        return
    }

    console.log("Connected correctly")

    const trustroot = client.db('trustroot')

    const checkError = (method, err) => {
        if (err) {
            console.error(`Error during ${method} operation ${err}`)
            client.close()
            return
        }
    }

    const users = trustroot.collection('users')
    users.deleteMany({ name: { $in: ['John', 'Mark'] } }, (err, results) => {

        checkError("deleteMany", err)
        console.log(`Delete completed: ${results}`)

        users.insertMany([
            { name: 'John', role: 'admin' },
            { name: 'Mark', role: 'user' }
        ], (err, result) => {

            checkError("insertMany", err)
            console.log(`InsertMany completed: ${util.inspect(result.ops, false, null, true)}`)

            users.findOne({ name: 'John' }, (err, item) => {

                checkError("findOne", err)
                console.log(`FindOne completed: ${util.inspect(item, false, null, true)}`)

                acl = new node_acl(new node_acl.mongodbBackend(trustroot, '_acl'))
                acl.allow([
                    {
                        roles: ['user'],
                        allows: [
                            {
                                resources: ['/api/events', '/api/categories'],
                                permissions: ['get', 'post', 'put', 'delete']
                            }
                        ]
                    },
                    {
                        roles: ['admin'],
                        allows: [
                            {
                                resources: ['/api/users'],
                                permissions: ['get', 'post', 'put', 'delete']
                            }
                        ]
                    }
                ], err => {

                    checkError("allow", err)
                    console.log(`Allow completed`)

                    acl.addRoleParents('admin', 'user', err => {

                        checkError("addRoleParents", err)
                        console.log(`AddRoleParents completed`)

                        const cursor = users.find({})

                        const onUser = user => {
                            if (user == null) {
                                console.log(`Find completed`)

                                console.log('Process ended correctly. Closing connection.')
                                client.close()
                                return
                            }

                            console.log(`Find one user: ${user.name}`)

                            console.log(`Inizio l'aggiunta dell'utente ${user.name} con ruolo ${user.role}`)
                            acl.addUserRoles(user._id.toString(), user.role, err => {
                                checkError("addUserRoles", err)
                                console.log(`Added ${user.role} role to user ${user.name} with id ${user._id}`)
                                cursor.next(onUser)
                            })
                        }

                        cursor.next(onUser)
                    })
                })
            })
        })
    })
})